export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'Riverman',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: "stylesheet",
                href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css",
            },
            {
                rel: "stylesheet",
                href: "https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css",
            },
            {
                rel: "stylesheet",
                href: "https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap",
            },
            {
                rel: "stylesheet",
                href: "//bizweb.dktcdn.net/100/119/255/themes/833679/assets/favicon.png?1630459471331",
            },
            {
                rel: "stylesheet",
                href: "https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css",
            },
            {
                rel: "stylesheet",
                href: "https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css",
            },

        ],
        script: [{
                src: "slick/slick.min.js",
            },
            {
                src: "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js",
            },
            {
                src: "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js",
            },
            {
                src: "https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js",
            },

            {
                src: "~/assets/js/back-top.js",
            },
            {
                src: "~/assets/js/wow.min.js",
            },
            {
                src: "~/assets/js/main.js",
            },
            {
                src: "https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js",
            },
            {
                src: "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js",
            },

        ],
    },
    loading: { color: "#000" },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        "~/assets/css/animate.css",
        "~/assets/css/base.css",
        "~/assets/css/grid.css",
        "~/assets/css/main.css",
        "~/assets/css/responsive.css",
        "~/assets/slick/slick-theme.css",
        "~/assets/slick/slick.css",
        "~/assets/fonts/fontawesome-free-5.15.4-web/css/all.min.css",

    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {}
}